from math import pi
def volumenCilindro(r, h):
    if  (isinstance(r, int) or isinstance(r,float)) and (isinstance(h,int) or isinstance(h,float)):
        area  =  pi*(r**2)*h
        return area, "m2"
