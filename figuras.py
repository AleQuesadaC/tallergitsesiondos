def area_triangulo(Base,Altura):
    if isinstance(Base,int) or (isinstance(Base,float)) and Base > 0:
        if isinstance(Altura,int) or isinstance(Altura,float) and Altura >0:
            return area_triangulo_aux(Base,Altura,0)
        else:
            return "La altura no es un número aceptado"
    else:
        return "La base no es un número aceptado"

def area_triangulo_aux(Base,Altura,Result):
    Result = (Base*Altura)/2
    return Result
